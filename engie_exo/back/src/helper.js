const fs = require('fs');
const path = require('path');

function readSchema() {
    return fs.readFileSync(
        path.join(__dirname, '../schema.graphql'),
        'utf8'
    )
}

module.exports = {
    readSchema
}