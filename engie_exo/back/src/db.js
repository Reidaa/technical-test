const mongoose = require("mongoose");

async function mongoConnect() {
	const docker_uri = "mongodb://moi:example@mongo:27017/db"
	const uri = "mongodb://moi:example@localhost:27017/db"
	let connected = false;

	try {
		console.log("Waiting for mongodb connection ...");
		await mongoose.connect(uri, undefined, undefined);
		connected = true
	} catch (error) {
		console.log(error)
	}

	if (connected === false) {
		try {
			console.log("Retry-ing with different connection")
			await mongoose.connect(docker_uri, undefined, undefined);
		} catch (error) {
			console.log(error)
            throw(error)
		}
	}

	console.log("DB connected")
}

async function mongoDisconnect() {
    await mongoose.disconnect()
    
    console.log("DB disconnected")
}

module.exports = {
    mongoConnect,
    mongoDisconnect
}
