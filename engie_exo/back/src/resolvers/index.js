const { UserMutation, UserQuery } = require("./user")

const rootResolver = {
    Query: {
        ...UserQuery,
    },
    Mutation: {
        ...UserMutation
    }
}

module.exports = rootResolver
