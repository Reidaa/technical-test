const { User } = require("../models/user");

const UserQuery = {
    users: async args => {
        try {
            const usersFetched = await User.find();
            return usersFetched.map(user => {
                return {
                    email: user.email,
                    _id: user.id,
                }
            })
        } catch (e) {
            throw e;
        }
    }
}

const UserMutation = {
    userCreate: async (_, { input }) => {
        try {
            const { email } = input;
            const doc = await User.create({ email })
            return {
                email: doc.email,
                _id: doc.id,
            }
        } catch (e) {
            throw e;
        }
    },

    userDelete: async (_, { input }) => {
        try  {
            const { id } = input;
            const res = await User.deleteOne({ "_id": id})
            if (res.deletedCount === 1)
                return true
            else
                Error("meeeh")
        } catch (error) {
            throw error;
        }
    },

    userUpdate: async (_, { input }) => {
        try {
            const { id, email } = input;
            const filter = { "_id": id }
            const update = { "email": email }
            const doc = await User.findOneAndUpdate(filter, update)
            return {
                email: doc.email,
                _id: doc.id,
            }
        } catch (error) {
            throw error;
        }
    }
}


module.exports = {
    UserMutation,
    UserQuery
}
