const { mongoConnect, mongoDisconnect } = require("./db")
const { startApolloServer } = require("./server")
const { readSchema } = require("./helper")
const GQLresolvers = require("./resolvers")


async function main() {
    await mongoConnect()
    await startApolloServer(readSchema(), GQLresolvers)
}


process.on('SIGTERM', async () => {
    console.log('Signal received.');
    await mongoDisconnect()

});

process.on('SIGINT', async () => {
    console.log("Caught interrupt signal");
    await mongoDisconnect()
});


main()
