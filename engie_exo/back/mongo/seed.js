const faker = require("faker")
const fs = require("fs");
const mongoose = require("mongoose")

const { mongoConnect, mongoDisconnect } = require("../src/db")
const { User } = require("../src/models/user")

async function importData(users) {
    try {
        await User.create(users);
        console.log("Data Imported...");
    } catch (err) {
        console.log(err);
    }
}

async function deleteData() {
    try {
        await User.deleteMany();
        console.log("Data Destroyed...");
    } catch (err) {
        console.log(err);
    }
}

async function seedDB() {
    await mongoConnect();

    const users = JSON.parse(
        fs.readFileSync(`${__dirname}/users.json`, "utf-8")
    );

    await deleteData();
    await importData(users);

    
    await mongoDisconnect();
}

seedDB()