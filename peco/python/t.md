# 1. Python Script 1:

Extract data from emails:

Sender: ABC@uk

Receiver: production@perenco

Data should include:
  - Sender details
  - Date and Time received
  - Attachments in the emails

Data then should be copied into a folder structure, for example:
`H:\Geoscience\Production_Data\Daily_Report`
Email then should be moved to folder name `ABC_vendor` in the email boxes

# 2. Python Script 2:

Download the `All offshore data` Shapefile from this [website](https://www.npd.no/en/about-us/open-data/) and unzip and export the file into different shapefiles:

- Simple coastline of the Norwegian and surrounding European nations
- Surface location of all Norwegian offshore wells
- Hydrocarbon fields for each country

# 3. Python Script 3:

Write a function that gets the path to `ABC daily report.xlsx` as an argument and returns a pandas dataframe containing
the file's data with the following structure:

| StartDateTime | EndDateTime | Volume (m3) | Mass (1000kg) | Density (kg/m3) |
|---------------|-------------|-------------|---------------|-----------------|
|               |             |             |               |                 |

If table from the previous exercise (now called ABC_readings) is stored in an SQL database. 
Write an SQL query that would pull the data and display it in the following way:

| StartDateTime | EndDateTime | Reading | ReadingUOM |
|---------------|-------------|---------|------------|
|               |             |         |            |
 	 	 	 

