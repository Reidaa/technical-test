import imaplib
import email
from datetime import datetime
from pathlib import Path
import csv
from dataclasses import dataclass


@dataclass(frozen=True)
class Arguments:
    sender: str
    receiver: str
    imap_server: str
    username: str
    password: str
    save_dirpath: Path
    moved_email_folder: str
    csv_filename: str


email_data: list[list[str]] = []
args = Arguments(
    sender='ABC@uk',
    receiver="production@perenco",
    imap_server='imap.example.com',
    username='email@example.com',
    password='password',
    save_dirpath=Path('H:/Geoscience/Production_Data/Daily_Report'),
    moved_email_folder='ABC_vendor',
    csv_filename='email_data.csv'
)

args.save_dirpath.mkdir(parents=True, exist_ok=True)

# Connect to the IMAP server
server = imaplib.IMAP4_SSL(args.imap_server)
server.login(args.username, args.password)
server.select('INBOX')

# Search for emails
status, messages = server.search(None, f"(FROM '{args.sender}') (TO '{args.receiver}')")
message_ids = messages[0].split()

# Iterate through the found emails
for msg_id in message_ids:
    # Fetch the email data
    status, data = server.fetch(msg_id, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_bytes(raw_email)

    # Extract sender details
    sender_name, sender_email = email.utils.parseaddr(email_message['From'])

    # Extract date and time received
    received_datetime = datetime.strptime(email_message['Date'], "%a, %d %b %Y %H:%M:%S %z").strftime(
        "%Y-%m-%d_%H-%M-%S")

    # Extract attachments
    attachment_filepaths: list[str] = []
    for part in email_message.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue

        if attachment_filename := part.get_filename():
            # Save attachment
            attachment_filepaths.append(attachment_filename)
            attachment_path = args.save_dirpath / attachment_filename
            with open(attachment_path, 'wb') as f:
                f.write(part.get_payload(decode=True))

    email_data.append(
        [str(msg_id), str(sender_email), str(sender_name), received_datetime, ",".join(attachment_filepaths)])

    # Move email to the specified folder
    server.copy(msg_id, args.moved_email_folder)
    server.store(msg_id, '+FLAGS', '\\Deleted')

# Permanently delete the marked emails
server.expunge()

# Disconnect from the server
server.close()
server.logout()

with open(csv_filepath := args.save_dirpath / args.csv_filename, 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['Message ID', 'Sender Email', 'Sender Name', 'Received at', 'Attachments Path'])
    writer.writerows(email_data)

print(f"Email data has been written to '{csv_filepath}'")
