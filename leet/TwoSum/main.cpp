#include <map>
#include <vector>
#include <iterator>

class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target) {
        std::map<int, int> m;
        std::vector<int> result;

        for (auto it = begin (nums); it != end (nums); ++it) {
            int idx = std::distance(nums.begin(), it);
            m.insert(std::make_pair(*it, idx));
            if (m.find(target - *it) != m.end()) {
                result.push_back(idx);
                result.push_back(m[target - *it]);
                return result;
            } 
        }
        return result;
    }
};