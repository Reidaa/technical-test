class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        map = {}
        for idx, value in enumerate(nums):
            map[value] = idx
            if (target - value) in map:
                return [idx, map[target - value]]


# 49ms