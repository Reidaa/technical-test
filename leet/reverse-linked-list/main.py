from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val: int = 0, next=None):
        self.val: int = val
        self.next: Optional[ListNode] = next


class Solution:

    """
    Runtime: 56ms, faster than 34.61% of Python3 online submissions for Reverse Linked List
    Memory Usage: 15.5 MB, less than 56,29% of Python online submissions for Reverse Linked List
    """
    @staticmethod
    def reverseList_1_iter(head: Optional[ListNode]) -> Optional[ListNode]:
        if head is None or head.next is None:
            return head
        prev = head

        nextt = head.next
        head.next = None
        while nextt is not None:
            tmp = nextt.next

            nextt.next = prev
            prev = nextt

            nextt = tmp
            print("i")

        return prev

    """
    Runtime: 42 ms, faster than 70.98% of Python3 online submissions for Reverse Linked List.
    Memory Usage: 15.4 MB, less than 94.25% of Python3 online submissions for Reverse Linked List.
    """
    @staticmethod
    def reverseList_2_iter(head: Optional[ListNode]) -> Optional[ListNode]:
        if head is None or head.next is None:
            return head

        curr = head
        prev = None

        while curr is not None:
            nextt = curr.next
            curr.next = prev
            prev = curr
            curr = nextt

        return prev

    """
    Runtime: 33 ms, faster than 94.07% of Python3 online submissions for Reverse Linked List.
    Memory Usage: 15.4 MB, less than 94.25% of Python3 online submissions for Reverse Linked List.
    """
    @staticmethod
    def reverseList_3_iter(head: Optional[ListNode]) -> Optional[ListNode]:
        curr = head
        prev = None

        while curr is not None:
            nextt = curr.next
            curr.next = prev
            prev = curr
            curr = nextt

        return prev

    @staticmethod
    def reverseList_1_rec(head: Optional[ListNode]) -> Optional[ListNode]:
        print("i")
        if head is None or head.next is None:
            return head
        head.next
        nextt = head.next
        # prev = head
        #
        # nextt.next = prev
        # prev = nextt

        return Solution.reverseList_1_rec(nextt)


def printLL(head: ListNode):
    now = head
    while now is not None:
        print(now.val)
        now = now.next


def main():
    # head = ListNode(1, ListNode(2, ListNode(3, ListNode(4, None))))
    # head = ListNode(1);
    head = None

    printLL(head)
    print("")
    # head = Solution.reverseList_1_iter(head)
    head = Solution.reverseList_2_iter(head)
    # head = Solution.reverseList_1_rec(head)
    print("")
    printLL(head)


if __name__ == "__main__":
    main()
