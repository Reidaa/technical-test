/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

#include <iostream>

//class ListNode
//{
//    public:
//        ListNode()
//        {
//            this->val = 0;
//            this->next = nullptr;
//        }
//
//        ListNode(int x)
//        {
//            this->val = x;
//            this->next = nullptr;
//        }
//
//        ListNode(int x, ListNode *next)
//        {
//            this->val = x;
//            this->next = next;
//        }
//
//        int val;
//        ListNode *next;
//};

struct ListNode
{
    int val;
    ListNode *next;

    ListNode() :
            val(0), next(nullptr)
    {}

    ListNode(int x) :
            val(x), next(nullptr)
    {}

    ListNode(int x, ListNode *next) :
            val(x), next(next)
    {}
};

void printLL(ListNode *head)
{
    ListNode *curr = head;

    while (curr != nullptr) {
        std::cout << curr->val << std::endl;
        curr = curr->next;
    }
}

class Solution
{
    public:

        /*
         * Runtime: 7 ms, faster than 73.35% of C++ online submissions for Reverse Linked List.
         * Memory Usage: 8.4 MB, less than 42.55% of C++ online submissions for Reverse Linked List.
         */
        static ListNode *reverseList_1(ListNode *head)
        {
            if ((head == nullptr) || (head->next == nullptr))
                return head;

            ListNode *tmp;
            ListNode *prev = head;
            ListNode *next = head->next;
            head->next = nullptr;

            while (next != nullptr) {
                tmp = next->next;

                next->next = prev;
                prev = next;

                next = tmp;
            }

            return prev;
        }

        static ListNode *reverseList_2(ListNode *head)
        {
            if ((head == nullptr) || (head->next == nullptr))
                return head;

            ListNode *curr = head;
            ListNode *prev = nullptr;
            ListNode *next;

            while (curr != nullptr) {
                next = curr->next;
                curr->next = prev;
                prev = curr;
                curr = next;
            }
            return prev;
        }

        static ListNode *reverseList_3(ListNode *head)
        {
            ListNode *curr = head;
            ListNode *prev = nullptr;
            ListNode *next;

            while (curr != nullptr) {
                next = curr->next;
                curr->next = prev;
                prev = curr;
                curr = next;
            }
            return prev;
        }

};

int main()
{
    ListNode i4 = ListNode(4);
    ListNode i3 = ListNode(3, &i4);
    ListNode i2 = ListNode(2, &i3);
    ListNode head = ListNode(1, &i2);

    printLL(&head);
    std::cout << std::endl;

    ListNode *truc = Solution::reverseList_3(&head);

    std::cout << std::endl;
    printLL(truc);
}