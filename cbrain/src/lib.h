#ifndef LIB_H
#define LIB_H

void selectionTri(float *arr, int n);
void init_tabs(float *tab_og, float *tab_f, int size);
void verif(float *tab_og, float *tab_f, int size);
void print_tab(float *tab, int size);

#endif