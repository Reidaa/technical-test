#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "lib.h"

void selectionTri(float *arr, int n)
{
	int i, j, min_idx;
	float tmp;

	// One by one move boundary of
	// unsorted subarray
	for (i = 0; i < n - 1; i++)
	{

		// Find the minimum element in
		// unsorted array
		min_idx = i;
		for (j = i + 1; j < n; j++)
			if (arr[j] < arr[min_idx])
				min_idx = j;

		// Swap the found minimum element
		// with the first element
		tmp = arr[min_idx];
		arr[min_idx] = arr[i];
		arr[i] = tmp;
		// swap(&arr[min_idx], &arr[i]);
	}
}

void init_tabs(float *tab_og, float *tab_f, int size)
{
	srand((unsigned int)time(NULL));
	float floor = 100.0;
	float r;

	for (int i = 0; i < size; i++)
	{
		r = ((float)rand() / (float)(RAND_MAX)*floor);
		tab_og[i] = r;
		tab_f[i] = r;
	}
	selectionTri(tab_og, size);
}

void verif(float *tab_og, float *tab_f, int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("%f %f\n", tab_og[i], tab_f[i]);
	}
}

void print_tab(float *tab, int size) {
	for (int is = 0; is < size; is++)
	{
		printf("%f\n", tab[is]);
	}
}
