#include <stdio.h>
#include <stdlib.h>
#include "lib.h"

#define TAILLE 10

void tri(float *tab, int size)
{
	int limit = size;
	int max_idx;
	float tmp;

	for (int i = 0; i < size; i++)
	{
        
		// print_tab(tab, size);

		max_idx = 0;
		for (int n = 0; n < limit; n++)
		{
			if (tab[max_idx] < tab[n])
				max_idx = n;
		}
		// printf("max value is %f\n", tab[max_idx]);
		
		tmp = tab[max_idx];
		tab[max_idx] = tab[size - (i + 1)];
		tab[size - (i + 1)] = tmp;

		limit = limit - 1;

		// printf("\n");
	}
}

int main(void)
{

	float *tab_orig = malloc(sizeof(float) * TAILLE);
	float *tab_final = malloc(sizeof(float) * TAILLE);

	init_tabs(tab_orig, tab_final, TAILLE);
	// print_tab(tab_final, TAILLE);
    // printf("\n");

	tri(tab_final, TAILLE);
	verif(tab_orig, tab_final, TAILLE);

	free(tab_orig);
	free(tab_final);
	return 0;
}