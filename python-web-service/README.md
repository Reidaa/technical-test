# Prérequis

 * Python >= 3.6 et virtualenv 
 * Docker et docker-compose

L'un des deux

# Installation

``` bash
$ ./install.sh
$ source venv/bin/activate
$ flask run
```

ou

```bash
docker-compose  -f "docker-compose.yml" up -d --build ws
```

# Routes disponible

```
/
```
Hello World
```
/user
```
Renvoie la liste de tous les utilisateurs trié par ordre croissant nom/prénom, leurs informations et le nombre d'utilisateurs.
```
/user?id=
```
Renvoie les informations de l'utilisateurs dont l'identifiant est égal a id.
```
/user/:id
```
Renvoie les informations de l'utilisateurs dont l'identifiant est égal a id.


# Reflexion

## Structure du projet
- [dockerfiles/](dockerfiles/), les fichiers docker.
- [requirements/](requirements/), les fichiers répertoriant les dépendances de l'application.
- [tests/](tests/), les modules de test.
- [wstt/](wstt/), le code du web service et le fichier CSV principale.
- [wstt/routes/](wstt/routes/), les routes du web service

## Choix de conception

J'ai essayé d'écrire le code le plus facilement extensible et maintenable.

- Modulariser chaque élément de mon application ([configuration](config.py), ["database"](wstt/db.py), [application](wstt), [routes](wstt/routes), [modèles](wstt/models.py)) afin d'isoler les méthodes associées et de pouvoir les tester indépendamment.

- Ouvrir et fermer le fichier à chaque utilisation de la ["database"](wstt/db.py), plus simple à mettre en oeuvre. 

- Ecrire deux [dockerfiles](dockerfiles/), un pour lancer l'application et un pour la tester.
    * Idéalement, j'aurais utilisé [run.dockerfile](dockerfiles/run.dockerfile) afin de produire des images docker que je déploierais sur le serveur en production.  
    * j'aurais utilisé [test.dockerfile](dockerfiles/test.dockerfile) pour automatiser le lancement des tests avant déploiement dans cette même chaine.

- Séparer les dépendances de mon application en deux fichiers ([common.txt](requirements/common.txt) et [dev.txt](requirements/dev.txt)) dans le but de ne pas installer des modules inutiles au fonctionnement de l'application.

- Créer un module [models.py](wstt/models.py) contenant une classe User afin de manipuler les données d'un utilisateur plus facilement.

- Plusieurs classes de configuration tirée de plusieurs cas de figures d'utilisation de l'application (Test, Production, Development)
    * Configuration extensible à l'aide d'un [.env](.env.template), .flaskenv ou de variable d'environnement (pour la configuration dans [docker-compose](docker-compose.yml))

# Propositions d'améliorations

- Mettre en place un vrai serveur compatible WSGI pour remplacer le serveur de développement fourni par flask.
- Ajouter un système d'authentification pour les utilisateurs.
- Ajouter des rôles aux utilisateurs (ADMIN, USER) afin de restreindre l'accès aux informations de la "database".
- Remplacer le fichier csv par une vraie base de données.
- Nettoyé les entrées du fichier CSV, par exemple enlever les espaces inutiles au début et à la fin de chaque nom et prénom. 
- Ajouter de la docs (beaucoup de docs).
