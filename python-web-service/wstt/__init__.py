from flask import Flask, request, json, jsonify, Response
from werkzeug.exceptions import HTTPException
from typing import Dict
import os

from .routes.user import userBlueprint


### Application Factory ###
def create_app(test_config: str = None):

    app = Flask(__name__)


    if test_config is not None:
        app.config.from_object(test_config)
    else:
        CONFIG_TYPE = os.getenv("CONFIG_TYPE", default="config.DevelopmentConfig")
        app.config.from_object(CONFIG_TYPE)

    @app.route("/")
    def index():
        return Response("Hello world", status=200)

    register_blueprints(app)

    register_error_handlers(app)

    return app


def register_blueprints(app: Flask):
    app.register_blueprint(userBlueprint)


def register_error_handlers(app: Flask):

    @app.errorhandler(HTTPException)
    def handle_exception(e):
        response = e.get_response()
        response.data = json.dumps({
            "code": e.code,
            "name": e.name,
            "description": e.description,
        })
        response.content_type = "application/json"
        return response
