def index_in_list(a_list, index) -> bool:
    """ Verifie si la liste possede bien l'index demandé"""
    return index < len(a_list)
