from typing import List, Optional
import os
from pathlib import Path
import csv


def is_file_empty(filepath: str) -> bool:
    result = os.stat(filepath).st_size == 0
    return result


def file_exist(filepath: str) -> bool:
    result = Path(filepath).exists()
    return result


class DB:
    class EmptyFileError(Exception):
        pass

    class FileNonexistenceError(Exception):
        pass

    def __init__(self, filepath: str = "wstt/user.csv") -> None:
        self._filepath = Path(filepath).resolve()

        if file_exist(filepath) is False:
            raise self.FileNonexistenceError()
        if is_file_empty(filepath) is True:
            raise self.EmptyFileError()

    def find_many_users(self) -> Optional[List[List[str]]]:
        with open(self._filepath) as csvfile:
            reader = csv.reader(csvfile, delimiter=";")
            rows = [row for row in reader]
            rows = rows[1:]
            sorted_rows = sorted(rows, key=lambda x: x[1] + x[2])
        return sorted_rows

    def find_unique_user(self, in_id: int) -> Optional[List[str]]:
        with open(self._filepath) as csvfile:
            reader = csv.reader(csvfile, delimiter=";")
            for row in reader:
                try:
                    user_id = int(row[0])
                    if in_id == user_id:
                        return row
                except ValueError:
                    pass
        return None
