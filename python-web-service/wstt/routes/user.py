from flask import Blueprint, request, jsonify, current_app

from ..db import DB
from ..models import User

userBlueprint = Blueprint("users", __name__, url_prefix="/user")


@userBlueprint.route("", methods=["GET"])
def user():
    try:
        db = DB(filepath=current_app.config["CSV_FILEPATH"])
    except DB.EmptyFileError:
        data = {"message": "Aucun adhérent n’est présent"}
        return jsonify(data)
    except DB.FileNonexistenceError:
        data = {"message": "Le fichier d’entrée est introuvable"}
        return jsonify(data), 404

    id = request.args.get("id", default=None, type=int)
    if id is not None:
        try:
            raw_user_info = db.find_unique_user(id)
            if raw_user_info is None:
                data = {"message": "Aucun adhérent ne correspond à votre demande"}
                return jsonify(data)
            user_info = User(raw_user_info).serialize()
            return user_info, 200
        except ValueError:
            pass
    else:
        raw_users_info = db.find_many_users()
        users_info = [User(raw_user_info).serialize() for raw_user_info in raw_users_info]
        if len(users_info) == 0:
            data = {"message": "Aucun adhérent n’est présent"}
            return jsonify(data)
        return jsonify({"number of users": len(users_info), "users": users_info})


@userBlueprint.route("/<id>")
def userByID(id: int):
    try:
        db = DB(filepath=current_app.config["CSV_FILEPATH"])
    except DB.EmptyFileError:
        data = {"message": "Aucun adhérent n’est présent"}
        return jsonify(data)
    except DB.FileNonexistenceError:
        data = {"message": "Le fichier d’entrée est introuvable"}
        return jsonify(data), 404

    if id is not None:
        id = int(id)
        try:
            raw_user_info = db.find_unique_user(id)
            if raw_user_info is None:
                data = {"message": "Aucun adhérent ne correspond à votre demande"}
                return jsonify(data)
            user_info = User(raw_user_info).serialize()
            return user_info, 200
        except ValueError:
            pass
