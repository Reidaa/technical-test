from typing import List

from .helper import index_in_list


class User:
    def __init__(self, info: List[str]):
        self.id: int = int(info[0]) if index_in_list(info, 0) else None
        self.lastname: str = info[1] if index_in_list(info, 1) else None
        self.firstname: str = info[2] if index_in_list(info, 2) else None
        self.phonenumber: str = info[3] if index_in_list(info, 3) else None

    def serialize(self):
        return {
            "identifiant": self.id,
            "nom": self.lastname,
            "prenom": self.firstname,
            "telephone": self.phonenumber
        }
