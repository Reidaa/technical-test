FROM python:3.10-slim

WORKDIR /app

COPY ./requirements/common.txt requirements/common.txt
COPY ./requirements/dev.txt requirements/dev.txt
RUN pip install -U pip && pip install -r requirements/common.txt -r requirements/dev.txt

COPY . .

ENTRYPOINT ["pytest", "tests"]
