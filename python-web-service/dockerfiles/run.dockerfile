FROM python:3.10-slim

WORKDIR /app

COPY ./requirements/common.txt requirements/common.txt
RUN pip install -U pip && pip install -r requirements/common.txt

COPY . .

RUN useradd ws
USER ws

# TODO: Remplacer par quelque de plus *production-ready*
ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]
