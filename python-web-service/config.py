#!/usr/bin/env python

import os
from dotenv import load_dotenv
load_dotenv()
from pathlib import Path

basedir = os.path.abspath(os.path.dirname(__file__))

class __Config:
    FLASK_ENV = os.getenv("FLASK_ENV", default="development")
    CSV_FILEPATH = None
    
    DEBUG = False
    TESTING = False
    
    JSON_AS_ASCII = False

class DevelopmentConfig(__Config):
    DEBUG = True
    CSV_FILEPATH = os.getenv("CSV_FILEPATH", default=f"{Path(basedir).joinpath('wstt/user.csv')}")

class TestingConfig(__Config):
    TESTING = True

class ProductionConfig(__Config):
    FLASK_ENV = "production"
    CSV_FILEPATH = os.getenv("CSV_FILEPATH", default=f"{Path(basedir).joinpath('wstt/user.csv')}")

