from wstt import create_app
from os import getenv

if __name__ == "__main__":
    app = create_app()
    port = getenv("PORT", 8080)
    app.run(host="0.0.0.0", port=port)
