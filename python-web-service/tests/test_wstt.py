import pytest

from wstt import create_app


@pytest.fixture
def client():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "wstt/user.csv"
    with app.test_client() as client:
        yield client


def test_index(client):
    rv = client.get("/")
    assert rv.status_code == 200


def test_users_2():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["number of users"] == 5


def test_users_3():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/3users.csv"
    with app.test_client() as client:
        rv = client.get("/user")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["number of users"] == 3


def test_users_4():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/no_user.csv"
    with app.test_client() as client:
        rv = client.get("/user")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["message"] == "Aucun adhérent n’est présent"


def test_user_id1():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user?id=1")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["identifiant"] == 1
        assert json_data["nom"] == "Bland"
        assert json_data["prenom"] == "Angie"
        assert json_data["telephone"] == "0611111111"


def test_user_id1_bis():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user/1")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["identifiant"] == 1
        assert json_data["nom"] == "Bland"
        assert json_data["prenom"] == "Angie"
        assert json_data["telephone"] == "0611111111"


def test_user_nonexistence():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user?id=5")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["message"] == "Aucun adhérent ne correspond à votre demande"


def test_user_nonexistence_bis():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user/5")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["message"] == "Aucun adhérent ne correspond à votre demande"


def test_user_id3():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user?id=3")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["identifiant"] == 3
        assert json_data["nom"] == "Williams"
        assert json_data["prenom"] == "Sherri"
        assert json_data["telephone"] == "0633333333"


def test_user_id3_bis():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user/3")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["identifiant"] == 3
        assert json_data["nom"] == "Williams"
        assert json_data["prenom"] == "Sherri"
        assert json_data["telephone"] == "0633333333"


def test_users():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/default.csv"
    with app.test_client() as client:
        rv = client.get("/user")
        assert rv.status_code == 200
        json_data = rv.get_json()
        assert json_data["number of users"] == 5
        users = json_data["users"]
        assert len(users) == 5

        assert users[0]["identifiant"] == 1
        assert users[0]["nom"] == "Bland"
        assert users[0]["prenom"] == "Angie"
        assert users[0]["telephone"] == "0611111111"

        assert users[1]["identifiant"] == 2
        assert users[1]["nom"] == "Doležalová"
        assert users[1]["prenom"] == "Michaela"
        assert users[1]["telephone"] == "0622222222"

        assert users[2]["identifiant"] == 4
        assert users[2]["nom"] == "Koutouxídou"
        assert users[2]["prenom"] == "Nikolétta"
        assert users[2]["telephone"] == "0644444444"

        assert users[3]["identifiant"] == 6
        assert users[3]["nom"] == "Vandesteene"
        assert users[3]["prenom"] == "Els"
        assert users[3]["telephone"] == "0655555555"

        assert users[4]["identifiant"] == 3
        assert users[4]["nom"] == "Williams"
        assert users[4]["prenom"] == "Sherri"
        assert users[4]["telephone"] == "0633333333"


def test_users_empty():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/empty.csv"
    with app.test_client() as client:
        rv = client.get("/user")
        assert rv.status_code == 200
        json_data = rv.get_json()
        message = json_data["message"]
        assert message == "Aucun adhérent n’est présent"


def test_users_nofile():
    app = create_app("config.TestingConfig")
    app.config["CSV_FILEPATH"] = "tests/data/nonexistence.csv"
    with app.test_client() as client:
        rv = client.get("/user")
        assert rv.status_code == 404
        json_data = rv.get_json()
        message = json_data["message"]
        assert message == "Le fichier d’entrée est introuvable"
