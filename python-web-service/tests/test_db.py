
import pytest

from wstt.db import DB


def test_empty():
    with pytest.raises(DB.EmptyFileError):
        DB(filepath="tests/data/empty.csv")


def test_nonexistence():
    with pytest.raises(DB.FileNonexistenceError):
        DB(filepath="tests/nonexistence.csv")
