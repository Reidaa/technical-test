Ecrire, dans un langage de votre choix (Java, C, C++, C#, PHP, …) une méthode qui parcourt un
arbre binaire et renvoie le numéro le plus grand qui est stocké dans les nœuds de l’arbre.

La signature de la méthode (en Java, par exemple) :
public int findMax(Node root) ;

Supposer l’interface suivante de la Class Node (en Java, par exemple) :
public int getValue() ;
public Node getRightChild() ;
public Node getLeftChild() ;

Nous ne souhaitons pas que vous implémentiez quoi que ce soit au-delà de la méthode findMax().
