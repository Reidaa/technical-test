import time
from typing import List


class Node:
    def __init__(self, value: int):
        self._left: Node | None = None
        self._right: Node | None = None
        self._value = value

    def PrintTree(self):
        print(self._value)

    def getValue(self):
        return self._value

    def getRightChild(self):
        return self._right

    def getLeftChild(self):
        return self._left

    def insertLeft(self, value: int):
        if self._left is None:
            self._left = Node(value)
        else:
            raise Exception()

    def insertRight(self, value: int):
        if self._right is None:
            self._right = Node(value)
        else:
            raise Exception


def setup() -> Node:
    root = Node(5)
    root.insertLeft(-12)
    root.insertRight(11)
    root.getLeftChild().insertLeft(3)
    root.getLeftChild().insertRight(19)
    root.getRightChild().insertLeft(-8)
    root.getRightChild().insertRight(7)
    root.getRightChild().getRightChild().insertLeft(11)
    root.getRightChild().getRightChild().insertRight(3)
    root.getRightChild().getRightChild().getRightChild().insertRight(-2)

    return root


def inOrderTraversal(root: Node):
    res = []
    if root:
        res = inOrderTraversal(root.getLeftChild())
        res.append(root.getValue())
        res = res + inOrderTraversal(root.getRightChild())
    return res


def preOrderTraversal(root: Node):
    res = []
    if root:
        res.append(root.getValue())
        res = res + preOrderTraversal(root.getLeftChild())
        res = res + preOrderTraversal(root.getRightChild())
    return res


def postOrderTraversal(root: Node):
    res = []
    if root:
        res = postOrderTraversal(root.getLeftChild())
        res = res + postOrderTraversal(root.getRightChild())
        res.append(root.getValue())
    return res


def getMax(root: Node):
    def inOrderTraversal(root: Node) -> list:
        res = []
        if root:
            res = inOrderTraversal(root.getLeftChild())
            res.append(root.getValue())
            res = res + inOrderTraversal(root.getRightChild())
        return res

    values = inOrderTraversal(root)
    return max(values)


def getMax2(root: Node) -> int:
    stack = []
    values = []
    now = root

    while True:
        if now is not None:
            stack.append(now)
            now = now.getLeftChild()
        elif len(stack) != 0:
            now = stack.pop()
            values.append(now.getValue())
            now = now.getRightChild()
        else:
            break

    return max(values)




if __name__ == "__main__":
    tree = setup()

    print(f"in order: {inOrderTraversal(tree)}")
    print(f"pre order: {preOrderTraversal(tree)}")
    print(f"post order: {postOrderTraversal(tree)}")

    print(f"max value = {getMax(tree)}")

    print(f"max value = {getMax2(tree)}")
    pass
