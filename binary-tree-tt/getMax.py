def getMax(root: Node) -> int:
    stack = []
    values = []
    now = root

    while True:
        if now is not None:
            stack.append(now)
            now = now.getLeftChild()
        elif len(stack) != 0:
            now = stack.pop()
            values.append(now.getValue())
            now = now.getRightChild()
        else:
            break

    return max(values)
